---
title: About me
comments: false
---

I'm a Computer Scientist currently based in Bath, UK. I have a BA (Hons) in Computer Science from the University of Cambridge; you'll find me up that way every now and again.

My main academic interest is in building secure systems: from specific constants and function calls up to the decision-making humans at the top of it all. In practice the systems I know most about are those on the Web.

I welcome PGP-encrypted mail: you can [find my PGP key here](/pgp.asc), or look up 0xE2500097D6E5FF39 on all the normal keyservers.
