---
title: Infinite looping zoom .gif generator in the browser
date: 2023-09-28
comments: false
---

<p>After a silly joke at work, I spent an evening implementing a Python script to generate "all the way down"-type animated gif images which endlessly zoom down onto some source image. I wanted this for a Slack emoji, but it'd be equally useful for Discord emoji and similar.</p>

<p>More specifically, design considerations were:</p>
<ol>
	<li>
		Avoid the pitfalls of other online generators which don't round properly. Depending whether the downscaled image had an odd or even number of pixels, the image would drift left and right through the animation. 
	</li>

	<li>
		Far too many .gif generators rely on server-side logic. I want to be able to play around with how my images are configured without incurring a bunch of network round trips, and without passing the images over to some third party service.
	</li>

	<li>
		I wanted proper support for transparency. This was basically just to generate a <a href="/img/turtlesallthewaydown.gif">"turtles all the way down" gif using the turtle emoji from Noto Color</a>.
	</li>

	<li>
		A desire to release it free to the world, for anyone to use (and modify).
	</li>
</ol>

The Python script met these needs at the time. However, I've had cases where I've wanted to generate these gifs but haven't been at a machine with <a href="https://python-pillow.org/">the Pillow library</a> installed. Thus a couple more evenings later...

<h2>All the way down in the browser</h2>
<p>Using the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API">Canvas API</a> and the <a href="https://jnordberg.github.io/gif.js/">gif.js</a> library</p>
<form onsubmit="allTheWayDown(this)" method="dialog">
	<input type="file" id="image" name="image" accept="image/*" />
	<input type="number" id="frames" name="frames" value="20" />
	<input type="number" id="scaleFactor" name="scaleFactor" value="16" step="1"/>
	<input type="number" id="frameTime" name="frameTime" step="5" value="50"/>
	<input type="submit" value="Take this all the way down!"/>
</form>
<img id="output" />

<script src="/js/gif.js"></script>
<script lang="javascript">
	// Adds the specified image to the centre of the canvas (based on the canvas's existing width and height) with the width and height specified
	function addCentredImage(ctx, image, width, height) {
		const deltaX = (ctx.canvas.width - width) / 2;
		const deltaY = (ctx.canvas.height - height) / 2;
		console.log(`Painting at (${deltaX}, ${deltaY})`);
		
		const downstepped = downStepImage(image, width, height);
		ctx.drawImage(
			downstepped,
			deltaX,
			deltaY,
			width,
			height);
	}
	
	// Scale an image down using 50% downstepping to avoid ugly scaling artifacts
	function downStepImage(image, width, height) {
		// base case: no down stepping required if it's less than a factor of two out, and just interpolate if we're going up
		if (width > image.width/2 || height > image.height/2) {
			return image;
		}
		
		// otherwise, create a new canvas of half the size and step it down
		const newCanvas = document.createElement('canvas');
		newCanvas.width = image.width/2;
		newCanvas.height = image.height/2;
		newCanvas.getContext('2d').drawImage(image, 0, 0, newCanvas.width, newCanvas.height);
		return downStepImage(newCanvas, width, height);
	}
	
	// Helper function to get image data from the whole canvas (to avoid needing to type getImageData a bunch of times)
	function getFullCanvasImageData(ctx) {
		return ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
	}

	function allTheWayDown(form) {
		const formData = new FormData(form);
		
		// Grab parameters from the form
		const imageFile = formData.get("image");
		const frames = Number(formData.get("frames"));
		const scaleFactor = Number(formData.get("scaleFactor"));
		const frameTime = Number(formData.get("frameTime"));
		
		// grab the canvas, ready to start splatting versions of the image on it
		const canvas = document.createElement('canvas');
		const ctx = canvas.getContext("2d");
		
		createImageBitmap(imageFile).then(async (origImgBitmap) => {
			// create the base image - we could compute a fixed point, but just doing some fixed number of recursions is fine for now
			// we do this by creating a regular canvas with the base image on it, and pasting the current version on top (scaled)
			canvas.width = origImgBitmap.width;
			canvas.height = origImgBitmap.height;
			
			// create the base version of the image
			addCentredImage(ctx, origImgBitmap, origImgBitmap.width, origImgBitmap.height);
			
			// TODO: Make this check if we're at a fixed point
			for (let i = 0; i < 5; i++ ) {
				const currentIteration = await createImageBitmap(getFullCanvasImageData(ctx));
				
				ctx.reset();
				addCentredImage(ctx, origImgBitmap, origImgBitmap.width, origImgBitmap.height);
				addCentredImage(ctx, currentIteration, currentIteration.width * scaleFactor ** -1, currentIteration.height * scaleFactor ** -1);
			}
			
			const iteratedImage = await createImageBitmap(getFullCanvasImageData(ctx));
				
			const gif = new GIF({
				workers: 4,
				workerScript: "/js/gif.worker.js",
			});

			// iterate from step=0 to step=frames, painting the base image shrunk by that number of steps over the original image grown by (frames - steps)
			// important that it's the original image, otherwise you end up with nasty halos when dealing with transparent images (as the low-res version bleeds out)
			for (const step of Array(frames).keys()) {
				const tempCanvas = document.createElement('canvas');
				tempCanvas.width = iteratedImage.width;
				tempCanvas.height = iteratedImage.height;

				const tempCtx = tempCanvas.getContext('2d');

				// TODO: work out exactly how many iterations are needed to fill the space
				// for the images I care about, 2 larger ones is enough.
				const LARGER_ITERATIONS = 2;

				for (const backgroundStep of Array(LARGER_ITERATIONS).keys()) {
					// these must be painted in reverse order, with the largest one first.
					// we paint the original image to avoid haloing,
					// otherwise we'd be painting a downscaled-then-upscaled
					// version which bleeds, even with the original image
					// overlaid later.
					const backgroundWidth = iteratedImage.width * (scaleFactor) ** ((step/frames) + LARGER_ITERATIONS - 1 - backgroundStep);
					const backgroundHeight = iteratedImage.height * (scaleFactor) ** ((step/frames) + LARGER_ITERATIONS - 1 - backgroundStep);
					addCentredImage(tempCtx, origImgBitmap, backgroundWidth, backgroundHeight);
				}

				// Finally paint the iterated image in the centre to go all the way down :)
				const smallWidth = iteratedImage.width * scaleFactor ** (step/frames - 1);
				const smallHeight = iteratedImage.height * scaleFactor ** (step/frames - 1);
				addCentredImage(tempCtx, iteratedImage, smallWidth, smallHeight);

				gif.addFrame(tempCanvas, {delay: frameTime});
			}
			
			gif.on('finished', function(blob) {
				const output = document.getElementById('output');
				const url = URL.createObjectURL(blob);
				output.setAttribute('src', url);
			});

			gif.render();
		});
	}
	</script>

	<p>Please enjoy, and use the created gifs responsibly!</p>